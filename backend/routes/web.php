<?php

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/list_event', function() {
    $results = app('db')->select("SELECT * FROM sciweek");
    return response()->json($results);
});

$router->put('/update_event', function(Illuminate\Http\Request $request){
    $event_name = $request->input("event_name");
    $event_datetime = strtotime($request->input("event_datetime"));
    $event_datetime = date( 'Y-m-d H:i:s',$event_datetime);
    $event_place = $request->input("event_place");
    $query = app('db')->update('UPDATE event_sciweek SET EventName = ?,
                                                   EventDate = ?,
                                                   Place = ?
                                                   WHERE EventID = 1',
                                                   [$event_name, 
                                                   $event_datetime,
                                                   $event_place]);

    return "OK";
});

$router->get('/list_accout', function() {
    $results = app('db')->select("SELECT * FROM accoutregister");
    return response()->json($results);
});

$router->post('/register_account', function(Illuminate\Http\Request $request) {
    $event_id = $request->input("event_id");
    $name = $request->input("name");
    $surname = $request->input("surname");

    /*$query = app('db')->insert('INSERT into accoutregister
                        (EventID , Name, Surname)
                        VALUES(?, ?, ?)',
                        [$event_id, $name, $surname] );*/
    return "ok";
});

$router->delete('/delete_event', function(Illuminate\Http\Request $equest) {
    $event_id = $request->input("event_id");
    $query = app('db')->delete('DELETE FROM sciweek WHERE EvnetID = ?', [$event_id] );
    return "ok";
});

